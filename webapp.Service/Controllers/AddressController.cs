﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using webapp.Service.Business;
using webapp.Service.Models;

namespace webapp.Service.Controllers
{
    public class AddressController : ApiController
    {
        ServiceResponse response;
        AddressBusiness business;

        public AddressController()
        {
            response = new ServiceResponse();
            business = new AddressBusiness();
        }

        private void Clean()
        {
            business.Dispose();
        }

        public ServiceResponse List()
        {
            try
            {
                response.Data = business.List();
                response.Status = 0;
                response.Message = "";
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            finally
            {
                Clean();
            }
            return response;
        }

        public ServiceResponse Save(Address address)
        {
            try
            {
                if(business.Save(address)==1)
                {
                    response.Status = 0;
                    response.Message = "Domicilio registrado correctamente";
                }
                else
                {
                    response.Status = 1;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            finally
            {
                Clean();
            }
            return response;
        }
    }
}
