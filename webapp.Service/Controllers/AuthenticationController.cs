﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using webapp.Service.Business;
using webapp.Service.Models;

namespace webapp.Service.Controllers
{
    public class AuthenticationController : ApiController
    {
        AuthenticationBusiness business;
        Credentials credentials;
        ServiceResponse response;

        public AuthenticationController()
        {
            business = new AuthenticationBusiness();
            credentials = new Credentials();
            response = new ServiceResponse();
        }

        public ServiceResponse Login(Credentials data)
        {
            try
            {
                data.Id = Guid.Empty;
                business.Login(data);
                if(data.Id.Equals(Guid.Empty))
                {
                    response.Status = 1;
                    response.Message = "Usuario y/o contraseña incorrectos";
                }
                else
                {
                    response.Status = 0;
                    response.Message = "Acceso correcto";
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            finally
            {
                business.Dispose();
            }
            return response;
        }

        public ServiceResponse Registration(RegistrationForm form)
        {
            try
            {
                if(business.Registration(form) == 1)
                {
                    response.Message = "Usuario registrado correctamente";
                    response.Status = 0;
                }
                else
                {
                    response.Message = "Ya existe un usuario registrado con el correo \""+form.Email+"\"";
                    response.Status = 1;
                }
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
            }
            finally
            {
                business.Dispose();
            }
            return response;
        }

        public ServiceResponse SignIn(RegistrationForm form)
        {
            try
            {
                form.Id = Guid.Empty;
                credentials.Email = form.Email;
                business.Find(credentials);
                if (credentials.Id.Equals(Guid.Empty))
                {
                    business.Register(form);
                    if (form.Id.Equals(Guid.Empty))
                    {
                        response.Message = "La operación no pudo ser completada";
                        response.Status = 1;
                    }
                    else
                    {
                        response.Message = "Usuario registrado";
                        response.Status = 0;
                    }
                }
                else
                {
                    response.Message = "La cuenta ya existe";
                    response.Status = 1;
                }
            }
            catch(Exception ex)
            {
                response.Message = ex.Message;
            }
            finally
            {
                business.Dispose();
                business = null;
            }
            return response;
        }
    }
}
