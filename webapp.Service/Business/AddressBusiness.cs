﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webapp.Repository;
using webapp.Service.Models;

namespace webapp.Service.Business
{
    public class AddressBusiness
    {
        AddressRepository<Address> addressRepository;
        public AddressBusiness()
        {
            addressRepository = new AddressRepository<Address>();
        }

        public void Dispose()
        {
            addressRepository.Dispose();
            addressRepository = null;
        }

        public List<Address> List()
        {
            return addressRepository.Get(null);
        }

        public int Save(Address address)
        {
            return addressRepository.Add(address);
        }
    }
}
