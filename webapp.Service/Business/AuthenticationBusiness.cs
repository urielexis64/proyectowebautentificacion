﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using webapp.Repository;
using webapp.Service.Models;

namespace webapp.Service.Business
{
    public class AuthenticationBusiness
    {
        AuthenticationRepository<RegistrationForm> authenticationRepository;
        CredentialRepository<Credentials> credentialRepository;
        UserRepository<Users> userRepository;
        Credentials credentials;
        Users users;
        List<Credentials> list;

        public AuthenticationBusiness()
        {
            authenticationRepository = new AuthenticationRepository<RegistrationForm>();
            credentialRepository = new CredentialRepository<Credentials>();
            credentials = new Credentials();
            userRepository = new UserRepository<Users>();
            users = new Users();
            list = null;
        }

        public void Dispose()
        {
            authenticationRepository.Dispose();
            authenticationRepository = null;
            credentialRepository.Dispose();
            credentialRepository = null;
            userRepository.Dispose();
            userRepository = null;
            if (list != null)
            {
                list.Clear();
                list = null;
            }
        }

        public void Find(Credentials data)
        {
            list = credentialRepository.Find(data);
            if (list.Count > 0)
            {
                data.Id = list[0].Id;
            }
        }

        public void Login(Credentials data)
        {
            list = credentialRepository.Get(data);
            if (list.Count > 0)
            {
                data.Id = list[0].Id;
            }
        }

        public void Register(RegistrationForm form)
        {
            users.id = Guid.NewGuid();
            users.firstname = form.Firstname;
            users.lastname = form.Lastname;
            if (userRepository.Add(users) > 0)
            {
                credentials.Id = users.id;
                credentials.Email = form.Email;
                credentials.Password = form.Password;
                if (credentialRepository.Add(credentials) > 0)
                {
                    form.Id = users.id;
                }
            }
        }

        public int Registration(RegistrationForm form)
        {
            return authenticationRepository.Register(form);
        }
    }
}
