﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webapp.Service.Models
{
    public class Credentials
    {
        public Credentials()
        {
            Email = "";
            Id = Guid.Empty;
            Password = "";
        }

        public string Email { get; set; }
        public Guid Id { get; set; }
        public string Password { get; set; }
    }
}
