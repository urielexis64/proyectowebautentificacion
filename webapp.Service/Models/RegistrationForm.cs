﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webapp.Service.Models
{
    public class RegistrationForm
    {
        public RegistrationForm()
        {
            Email = "";
            Firstname = "";
            Id = Guid.Empty;
            Lastname = "";
            Password = "";
        }

        public string Email { get; set; }
        public string Firstname { get; set; }
        public Guid Id { get; set; }
        public string Lastname { get; set; }
        public string Password { get; set; }
    }
}
