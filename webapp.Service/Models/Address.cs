﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webapp.Service.Models
{
    public class Address
    {
        public Address()
        {
            HouseNumber = "";
            PostalCode = "";
            Street = "";
            Suburb = "";
        }

        public string HouseNumber { get; set; }
        public string PostalCode { get; set; }
        public string Street { get; set; }
        public string Suburb { get; set; }
    }
}
