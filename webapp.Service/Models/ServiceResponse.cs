﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webapp.Service.Models
{
    public class ServiceResponse
    {
        public ServiceResponse()
        {
            Data = null;
            Message = "Error interno del sistema";
            Status = -1;
        }

        public object Data { get; set; }
        public string Message { get; set; }
        public int Status { get; set; }
    }
}
