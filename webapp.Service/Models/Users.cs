﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webapp.Service.Models
{
    public class Users
    {
        public Users()
        {
            firstname = "";
            id = Guid.Empty;
            lastname = "";
        }

        public string firstname { get; set; }
        public Guid id { get; set; }
        public string lastname { get; set; }
    }
}
