﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace webapp.Service.Filters
{
    public class ApiTransferFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(
            HttpActionExecutedContext context)
        {
        }

        public override void OnActionExecuting(
            HttpActionContext context)
        {
        }
    }
}
