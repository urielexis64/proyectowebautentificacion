﻿using libx.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webapp.Repository
{
    public class AddressRepository<T> : RepositoryBase<T>
    {
        public AddressRepository()
        {
            SetConnection("development");
            SetCommand("[dbo].[user_address]");
        }
    }
}
