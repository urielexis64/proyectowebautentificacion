﻿using libx.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webapp.Repository
{
    public class CredentialRepository<T> : RepositoryBase<T>
    {
        public CredentialRepository()
        {
            SetConnection("development");
            SetCommand("[dbo].[credentials]");
        }

        public List<T> Find(T data)
        {
            return Retrieve(5, data);
        }
    }
}
