﻿using libx.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webapp.Repository
{
    public class UserRepository<T> : RepositoryBase<T>
    {
        public UserRepository()
        {
            SetConnection("development");
            SetCommand("[dbo].[users]");
        }
    }
}
