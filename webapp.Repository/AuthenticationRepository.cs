﻿using libx.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace webapp.Repository
{
    public class AuthenticationRepository<T> : RepositoryBase<T>
    {
        public AuthenticationRepository()
        {
        }

        public int Register(T data)
        {
            SetConnection("development");
            SetCommand("[dbo].[registro]");
            return Post(5, data);
        }
    }
}
