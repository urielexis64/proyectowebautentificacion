﻿$(function () {
    $('.modal').modal();
    $('.btn-save').click(function () {
        var json = new Object();
        json.street = $('#street').val();
        json.houseNumber = $('#house-number').val();
        json.suburb = $('#suburb').val();
        json.postalCode = $('#postal-code').val();
        enviar(json);
    });
    $('.btn.modal-trigger').click(function () {
        limpiar();
    });
    obtener();
});

function enviar(json) {
    var uri = $('form').prop('action');
    console.log(json);
    $.ajax({
        url: uri
        , data: json
        , method: 'post'
    }).done(function (response) {
        if (response.status == 0) {
            obtener();
            $('.modal').modal('close');
        } else {
            alert(response.message);
        }
    }).fail(function () { });
}

function limpiar() {
    $('#street').val('');
    $('#house-number').val('');
    $('#suburb').val('');
    $('#postal-code').val('');
}

function obtener() {
    var uri = $('#address-uri').val();
    var row = '';
    $.ajax({
        url: uri
        , method: 'post'
    }).done(function (response) {
        if (response.status == 0) {
            $('#address-list tbody').html('');
            $.each(response.data, function (index, value) {
                row = '<tr>' +
                    '<td> ' + value.street + '</td>' +
                    '<td>' + value.houseNumber + '</td>' +
                    '<td>' + value.suburb + '</td>' +
                    '<td>' + value.postalCode + '</td></tr>';
                $('#address-list tbody').append(row);
            });
        }
    }).fail(function () { });
}