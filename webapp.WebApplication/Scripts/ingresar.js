﻿$(function () {
    $('.modal').modal();
    $('.btn').click(function () {
        var json = new Object();
        json.password = $('#password').val();
        json.email = $('#email').val();
        console.log($('form').prop('action'));
        enviar(json);
    });
});

function enviar(json) {
    var uri = $('form').prop('action');
    console.log(json);
    $.ajax({
        url: uri
        , data: json
        , method: 'post'
    }).done(function (response) {
        document.getElementById("dialogMessage").innerHTML = response.message;
        if (response.status == 0) {
            document.getElementById("dialogMessage").style = "color: #00FF00";
        } else {
            document.getElementById("dialogMessage").style = "color: #FF0000";
        }
    }).fail(function () { });
}