﻿using System.Web.Optimization;

namespace webapp.WebApplication
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Main Layout
            bundles.Add(new StyleBundle("~/css/main-layout").Include(
                      "~/Content/main-layout.css"));
            bundles.Add(new ScriptBundle("~/js/main-layout").Include(
                        "~/Scripts/main-layout.js"));
            #endregion
            #region Ingresar
            bundles.Add(new ScriptBundle("~/js/ingresar").Include(
                "~/Scripts/ingresar.js"));
            #endregion
            #region Registrarse
            bundles.Add(new ScriptBundle("~/js/registrarse").Include(
                "~/Scripts/registrarse.js"));
            #endregion
            #region Direcciones
            bundles.Add(new ScriptBundle("~/js/direcciones").Include(
                "~/Scripts/direcciones.js"));
            #endregion
        }
    }
}