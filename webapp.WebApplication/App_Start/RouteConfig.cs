﻿using System.Web.Mvc;
using System.Web.Routing;

namespace webapp.WebApplication
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Default",
                url: "{action}/{id}",
                defaults: new { controller = "Main", action = "inicio", id = UrlParameter.Optional }
            );
        }
    }
}