﻿using System.Web.Http.Filters;
using System.Web.Mvc;
using webapp.Service.Filters;

namespace webapp.WebApplication
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters, HttpFilterCollection api)
        {
            filters.Add(new HandleErrorAttribute());
            api.Add(new ApiTransferFilter());
        }
    }
}