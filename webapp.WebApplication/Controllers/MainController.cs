﻿using System.Web.Mvc;

namespace webapp.WebApplication.Controllers
{
    public class MainController : Controller
    {
        public ActionResult Inicio()
        {
            return View();
        }

        public ActionResult Direcciones()
        {
            return View();
        }

        public ActionResult Ingresar()
        {
            return View();
        }

        public ActionResult Registrarse()
        {
            return View();
        }
    }
}